## Installation and project running

#### 1. Install [python3](https://www.python.org/downloads/release/python-394/) v.3.9.4
#### 2. Install [Chrome browser](https://sites.google.com/a/chromium.org/chromedriver/downloads) v.92.0.4515.107
#### 3. Open the project and add [venv](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html#existing-environment) in [PyCharm](https://www.jetbrains.com/pycharm/download/#section=mac)
#### 4. Run in terminal "pip install selenium==3.141.0 webdriver-manager==3.4.2 pytest==6.2.4"
#### 5. Go to the project folder
#### 6. Run in terminal "python -m pytest tests/test_main.py -v"