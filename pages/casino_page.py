from selenium.webdriver.common.by import By

from .base_element import BaseElement
from .base_page import BasePage
from .locator import Locator


class CasinoPage(BasePage):

    # Perfect way is saving the urls in config file for different environments
    url = 'https://www.hajper.com/sv/casino/explore'

    @property
    def casino_details_buttons(self):
        locator = Locator(by=By.CSS_SELECTOR, value="button[data-at='game-support-menu-button']")
        return BaseElement(self.driver, locator)

    @property
    def click_details_button(self):
        locator = Locator(by=By.CSS_SELECTOR, value="button[data-at='game-support-menu-button']")
        return BaseElement(self.driver, locator).click()

        return None

    @property
    def wish_button(self):
        locator = Locator(by=By.CSS_SELECTOR, value=".game-info__love-button")
        return BaseElement(self.driver, locator)

    @property
    def click_wish_button(self):
        locator = Locator(by=By.CSS_SELECTOR, value=".game-info__love-button")
        BaseElement(self.driver, locator).click()

        return None

    @property
    def wish_notification(self):
        locator = Locator(by=By.XPATH, value="//div[@data-at='notification-bar-header']")
        return BaseElement(self.driver, locator)
