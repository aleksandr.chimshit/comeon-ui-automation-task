from selenium.webdriver.common.by import By

from .base_element import BaseElement
from .base_page import BasePage
from .locator import Locator


class MainPage(BasePage):

    # Perfect way is saving the urls in config file for different environments
    url = 'https://www.hajper.com/'

    @property
    def header_buttons(self):
        locator = Locator(by=By.XPATH, value="//div[@data-at='regulation-header-wrapper']/span")
        return BaseElement(self.driver, locator)

    @property
    def spelpaus_button(self):
        locator = Locator(by=By.CSS_SELECTOR, value="a[data-at='regulation-header-spel-paus']")
        return BaseElement(self.driver, locator)

    @property
    def responsibles_button(self):
        locator = Locator(by=By.CSS_SELECTOR, value="a[data-at='regulation-header-responsible-gaming']")
        return BaseElement(self.driver, locator)

    @property
    def selftest_button(self):
        locator = Locator(by=By.CSS_SELECTOR, value="a[data-at='regulation-header-self-test']")
        return BaseElement(self.driver, locator)

    @property
    def spelinspect_logo(self):
        locator = Locator(by=By.CSS_SELECTOR, value="span[data-at='spelinspektionen-logo']")
        return BaseElement(self.driver, locator)

    @property
    def casino_icon(self):
        locator = Locator(by=By.CSS_SELECTOR, value="a[data-at='product-casino']")
        return BaseElement(self.driver, locator)

    @property
    def click_casino_icon(self):
        locator = Locator(by=By.CSS_SELECTOR, value="a[data-at='product-casino']")
        BaseElement(self.driver, locator).click()

    @property
    def close_cookie_notification(self):
        locator = Locator(by=By.XPATH, value="//button[@id='']")
        BaseElement(self.driver, locator).click()
