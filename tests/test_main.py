from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from pages.main_page import MainPage
from pages.casino_page import CasinoPage


class MainTests:

    def test_page_loading(self):
        browser = webdriver.Chrome(ChromeDriverManager().install())

        # Initiate method for checking site loading during 10 seconds
        browser.set_page_load_timeout(10)
        main_page_test = MainPage(driver=browser)

        # Open the site
        main_page_test.go()

        browser.quit()

    def test_headers_buttons_1(self):
        browser = webdriver.Chrome(ChromeDriverManager().install())
        main_page_test = MainPage(driver=browser)

        # Open the site
        main_page_test.go()

        # Check visibility of first button from header
        main_page_test.spelpaus_button

        # Check visibility of second button from header
        main_page_test.responsibles_button

        # Check visibility of third button from header
        main_page_test.selftest_button

    # This is more elegant way with objects iteration.
    # By the way there is too much boilerplate code inside the test case.
    def test_headers_buttons_2(self):
        browser = webdriver.Chrome(ChromeDriverManager().install())
        main_page_test = MainPage(driver=browser)

        # Open the site
        main_page_test.go()

        elements = browser.find_elements_by_xpath("//div[@data-at='regulation-header-wrapper']/span")

        for button in elements:
            assert button.is_displayed() == True

    def test_inspektionen_logo(self):
        browser = webdriver.Chrome(ChromeDriverManager().install())
        main_page_test = MainPage(driver=browser)

        # Open the site
        main_page_test.go()

        # Check visibility of inspektionen logo
        main_page_test.spelinspect_logo

    def test_wishlist_notification(self):
        browser = webdriver.Chrome(ChromeDriverManager().install())
        main = MainPage(driver=browser)
        casino = CasinoPage(driver=browser)

        # Open the site
        main.go()

        # Click "Accept" for closing cookie notification.
        main.close_cookie_notification #Not a best way because I know that the js code can do it extremely straightforward. But it was the quick decision.

        # Step 3
        main.click_casino_icon

        # Step 4
        casino.click_details_button

        # Step 5
        casino.click_wish_button

        # Step 6 - check visibility of the wishlist notification for not logged in users
        casino.wish_notification
